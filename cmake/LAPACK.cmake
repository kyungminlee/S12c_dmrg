if (MSVC)
    if (NOT DEFINED INTEL_ROOT)
        message(FATAL_ERROR "Please set the variable INTEL_ROOT (e.g. C:\\Program Files (x86)\\IntelSWTools\\compilers_and_libraries\\windows) to the directory containing mkl, tbb, compiler and redist")
    endif()
    include_directories("${INTEL_ROOT}\\mkl\\include")
    set(MKL_LIBNAMES mkl_intel_lp64.lib mkl_intel_thread.lib mkl_core libiomp5md) # Static iomp
    foreach(LIBNAME ${MKL_LIBNAMES}) 
        unset(LIB CACHE)
        find_library(LIB NAMES "${LIBNAME}"
                     PATHS
                        "${INTEL_ROOT}\\tbb"
                        "${INTEL_ROOT}\\mkl"
                        "${INTEL_ROOT}\\compiler"
                     PATH_SUFFIXES
                        "lib"
                        "lib\\intel64"
                        "lib\\intel64\\vc14"
                     )
        list(APPEND LIBS "${LIB}")
    endforeach(LIBNAME)

    set(INTEL_REDIST_DIR "${INTEL_ROOT}\\redist\\intel64")
    add_custom_command(TARGET main POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${INTEL_REDIST_DIR}\\compiler\\libiomp5md.dll" $<TARGET_FILE_DIR:main>
            )
    set(BLAS_LIBRARIES "${LIBS}")
    set(LAPACK_LIBRARIES "${LIBS}")
    set(BLAS_FOUND TRUE)
    set(LAPACK_FOUND TRUE)
else()
    find_package(LAPACK REQUIRED)
endif()
