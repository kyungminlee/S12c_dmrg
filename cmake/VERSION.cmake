set(VERSION_FILE "${CMAKE_SOURCE_DIR}/src/version.cc")

find_package(Git)
if (Git_FOUND)
    message("Git found: ${GIT_EXECUTABLE}")
    execute_process(COMMAND "${GIT_EXECUTABLE}" log --pretty=format:%h -n 1
                    OUTPUT_VARIABLE GIT_REV
                    ERROR_QUIET)
else()
    set(GIT_REV "unknown")
endif()

set(VERSION "const char* GIT_REV=\"${GIT_REV}\";")

if(EXISTS "${VERSION_FILE}")
    file(READ "${VERSION_FILE}" VERSION_)
else()
    set(VERSION_ "")
endif()

if (NOT "${VERSION}" STREQUAL "${VERSION_}")
    file(WRITE "${VERSION_FILE}" "${VERSION}")
endif()
