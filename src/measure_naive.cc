#include <iostream>
#include <filesystem>
#include <memory>

#include <xtensor/xtensor.hpp>
#include <xtensor/xtensor_simd.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xnpy.hpp>

#include "CLI11.hpp"
#include "cpptoml.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "ProgressBar.hpp"

#include <itensor/all.h>
#undef Error

#include "s12c.h"

using namespace s12c;

int measure() {
    namespace IT = itensor;
    using namespace itensor;
    using std::filesystem::is_regular_file;

    if (!is_regular_file("sites.itn") || !is_regular_file("psi.itn")) {
        spdlog::error("File does not exist");
        return 1;
    }

    IT::SpinHalf sites;
    IT::MPS psi;
    spdlog::info("Reading from file sites.itn");
    readFromFile("sites.itn", sites);

    Int n_sites = sites.length();
    spdlog::info("n_sites = {}", n_sites);

    psi = IT::MPS(sites);

    spdlog::info("Reading from file psi.itn");
    readFromFile("psi.itn", psi);

    // Si_mu, Sj_nu
    using shape_type = xt::xarray<double>::shape_type;
    
    //auto hamiltonian = s12c::make_hamiltonian(sites, bonds, J1, J2, Jc);
	spdlog::info("Measuring spin");
	xt::xarray<Real> spin_expectations = xt::zeros<Real>({size_t(n_sites),});
	{
		ProgressBar progress_bar(n_sites, 70);
		for (Int i = 0; i < n_sites; ++i) {
			auto ampo = AutoMPO(sites);
			ampo += "Sz", (i + 1);
			auto O = toMPO(ampo);
			spin_expectations(i) = std::real(innerC(psi, O, psi));
			++progress_bar;
			progress_bar.display();
		}
		std::cerr << std::endl;
	}
	spdlog::info("Finished");

	spdlog::info("Measuring spin-spin");
	xt::xarray<Real> spin_spin_expectations = xt::zeros<Real>({size_t(n_sites), size_t(n_sites)});
	{
		ProgressBar progress_bar(n_sites * n_sites, 70);
		for (Int i = 0; i < n_sites; ++i) {
			for (Int j = i; j < n_sites; ++j) {
				auto ampo = AutoMPO(sites);
				ampo += "Sz", (i + 1), "Sz", (j + 1);
				auto O = toMPO(ampo);
				auto val = std::real(innerC(psi, O, psi));
				spin_spin_expectations(i, j) = val;
				spin_spin_expectations(j, i) = val;

				++progress_bar;
				progress_bar.display();
			}
		}
		std::cerr << std::endl;
	}
	spdlog::info("Finished");

	std::cout << "Spin\n";
    std::cout << spin_expectations << std::endl;
    std::cout << "Spin-Spin\n";
    std::cout << spin_spin_expectations << std::endl;

	xt::dump_npy("result-spin.npy", spin_expectations);
	xt::dump_npy("result-spin-spin.npy", spin_spin_expectations);

    return 0;
}

int main(int argc, char **argv) {
    auto console = spdlog::stdout_color_mt("console");
    //auto file_logger = spdlog::basic_logger_mt("basic_logger", "logs.txt");
    namespace IT = itensor;

    CLI::App app{"measure"};
    {
        CLI11_PARSE(app, argc, argv);
    }

    measure();

    return 0;
}

