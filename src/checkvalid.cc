#include <iostream>
#include <filesystem>
#include <xtensor/xtensor.hpp>
#include <xtensor/xtensor_simd.hpp>
#include "CLI11.hpp"
#include "cpptoml.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

//#include "Yaml.hpp"
#include "cpptoml.h"

#include <itensor/all.h>
#undef Error

#include "s12c.h"

using namespace s12c;

extern const char* GIT_REV;

int test(std::string sites_filename, std::string psi_filename);
int unsafe_main(int argc, char **argv);


int unsafe_main(int argc, char **argv)
{
    namespace IT = itensor;
    auto console = spdlog::stdout_color_mt("console");

    std::string sites_filename = "sites.itn";
    std::string psi_filename = "psi.itn";

    {
        CLI::App app{std::string("S12c_dmrg, version git-") + GIT_REV};
        bool version = false;
        app.add_option("sites_file", sites_filename, "system toml")->required(true);
        app.add_option("psi_file", psi_filename, "Wave function file")->required(true);
        CLI11_PARSE(app, argc, argv);
    }

    int err = test(sites_filename, psi_filename);
    spdlog::info("Status {}", err);
    spdlog::info("Finished");
    return err;
}


int test(std::string sites_filename, std::string psi_filename) {

    namespace IT = itensor;

    using std::filesystem::is_regular_file;

    if (!is_regular_file(sites_filename) || !is_regular_file(psi_filename)) {
      return 1;
    }

    spdlog::info("Reading from file {}", sites_filename);

    IT::SpinHalf sites;
    readFromFile(sites_filename, sites);

    spdlog::info("Reading from file {}", psi_filename);
    IT::MPS psi = IT::MPS(sites);
    readFromFile(psi_filename, psi);

    IT::AutoMPO ampo(sites);
    IT::MPO mpo = toMPO(ampo);
    return 0;
}

int main(int argc, char **argv) {
  try {
    return unsafe_main(argc, argv);
  } catch (std::exception const & e) {
    std::cerr << e.what() << std::endl;
    throw e;
  }
}

