#include <fstream>
#include <iostream>
#include <filesystem>
#include <memory>
#include <exception>
#include <typeinfo>

#include <xtensor/xtensor.hpp>
#include <xtensor/xtensor_simd.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xnpy.hpp>
#include <xtensor/xcsv.hpp>

#include "CLI11.hpp"
#include "cpptoml.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "ProgressBar.hpp"

#include <itensor/all.h>
#undef Error

#include "s12c.h"

using namespace s12c;
namespace IT = itensor;

extern const char* GIT_REV;

auto read(std::string const & sites_filename,
          std::string const & psi_filename) {
    using namespace itensor;
    using std::filesystem::is_regular_file;

//if (!is_regular_file("sites.itn") || !is_regular_file("psi.itn")) {
    if (!is_regular_file(sites_filename) || !is_regular_file(psi_filename)) {
        spdlog::error("File does not exist");
		throw std::runtime_error("File does not exist");
    }

    IT::SpinHalf sites;
    spdlog::info("Reading from file {}", sites_filename);
    readFromFile(sites_filename, sites);

    Int n_sites = sites.length();
    spdlog::info("n_sites = {}", n_sites);

    IT::MPS psi(sites);

    spdlog::info("Reading from file {}", psi_filename);
    readFromFile(psi_filename, psi);
	spdlog::info("Finished");

	return std::make_tuple(sites, psi);
}


auto measure(IT::SpinHalf & sites, IT::MPS & psi) {
	using namespace itensor;
	size_t n_sites = sites.length();
	using shape_type = xt::xarray<Real>::shape_type;
	
	xt::xarray<Real> spin_expectation = xt::zeros<Real>(shape_type{n_sites,});
	xt::xarray<Real> spin_spin_expectation = xt::zeros<Real>(shape_type{n_sites, n_sites,});

	ProgressBar progress_bar(n_sites, 70);
	for (Int i = 0 ; i < n_sites ; ++i) {
		spin_spin_expectation(i, i) = 0.25;
		ITensor S_i = op(sites, "Sz", i+1);
	
		psi.position(i+1);
		
		ITensor spin_value = (psi(i+1) * S_i) * dag(prime(psi(i+1), "Site"));
		spin_expectation(i) = std::real(eltC(spin_value));

		MPS psidag = dag(psi);
		psidag.prime("Link");

		Index link_i = leftLinkIndex(psi, i+1);

		ITensor C = prime(psi(i+1), link_i);

		C *= S_i;
		C *= prime(psidag(i+1), "Site");

		for (Int j = i+1 ; j < n_sites ; ++j) {
			ITensor S_j = op(sites, "Sz", j+1);
			Index link_j = rightLinkIndex(psi, j+1);

			ITensor D = C;
			D *= (prime(psi(j+1), link_j) * S_j);
			D *= prime(psidag(j+1), "Site");
			auto result = eltC(D);
			
			spin_spin_expectation(i, j) = std::real(result);
			spin_spin_expectation(j, i) = std::real(result);

			C *= psi(j+1);
			C *= psidag(j+1);
		}
		++progress_bar;
		progress_bar.display();
	}
	std::cerr << std::endl;
	return std::make_tuple(spin_expectation, spin_spin_expectation);
}


int unsafe_main(int argc, char **argv) {
    auto console = spdlog::stdout_color_mt("console");
    namespace IT = itensor;

    {
	    CLI::App app{std::string("measure, version git-") + GIT_REV};
		bool version = false;
        app.add_flag("-v,--version", version, "Show version");		
        CLI11_PARSE(app, argc, argv);
        if (version) {
            fmt::print("{}, version git-{}\n", argv[0], GIT_REV);
            return 0;
        }
    }

    spdlog::info("Start reading");

    std::vector<std::tuple<int, std::string>> psi_filenames{ std::make_tuple(0, std::string("psi.itn")) };

    for (int i = 1 ; i < 100 ; ++i) {
      std::string psi_filename = fmt::format("psi_{:02d}.itn", i);
      if (std::filesystem::is_regular_file(psi_filename)) {
        spdlog::info("Adding {} to the list", psi_filename);
        psi_filenames.emplace_back(i, psi_filename);
      } else {
        break;
      }
    }
    spdlog::info("Total {} wave functions", psi_filenames.size());

    for (auto [i, psi_filename] : psi_filenames) {
      //std::string psi_filename = "psi.itn";
      auto [sites, psi] = read("sites.itn", psi_filename);
      spdlog::info("Finished reading");

      spdlog::info("Start measuring");
      auto [Sz, SzSz] = measure(sites, psi);
      spdlog::info("Finished measure");

      spdlog::info("Saving spin");
      {
        std::string result_filename = fmt::format("result-spin-{:02d}.npy", i);
        xt::dump_npy(result_filename, Sz);
      }
      spdlog::info("Saving spin-spin");
      {
        std::string result_filename = fmt::format("result-spin-spin-{:02d}.npy", i);
        xt::dump_npy(result_filename, SzSz);
      }
      spdlog::info("Finished");
    }

    return 0;
}



int main(int argc, char **argv) {
  try {
    return unsafe_main(argc, argv);
  } catch (std::exception const & e) {
    std::cerr << e.what() << std::endl;
    throw e;
  }
}

