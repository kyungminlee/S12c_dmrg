#include <iostream>
#include <filesystem>
#include <xtensor/xtensor.hpp>
#include <xtensor/xtensor_simd.hpp>
#include "CLI11.hpp"
#include "cpptoml.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

//#include "Yaml.hpp"
#include "cpptoml.h"

#include <itensor/all.h>
#undef Error

#include "s12c.h"

using namespace s12c;

extern const char* GIT_REV;


LatticeParameter
read_lattice(cpptoml::table const & input_lattice);

HamiltonianParameter
read_hamiltonian(cpptoml::table const & input_hamiltonian);

RunParameter
read_run(cpptoml::table const & input_run);

itensor::Sweeps
convert_sweeps(std::vector<SweepParameter> const & sweeps_param);

void
run(LatticeParameter const & lattice_parameter,
    HamiltonianParameter const & hamiltonian_parameter,
    RunParameter const & run_parameter,
    Int n_excited);

void show(LatticeParameter const & lattice_parameter) {
    Int n1 = lattice_parameter.n1;
    Int n2 = lattice_parameter.n2;
    bool p1 = lattice_parameter.periodic1;
    bool p2 = lattice_parameter.periodic2;
    
    spdlog::info("n1 = {}, n2 = {}, periodic1 = {}, periodic2 = {}", n1, n2, p1, p2);
}

void show(HamiltonianParameter const & hamiltonian_parameter) {
    Real J1 = hamiltonian_parameter.J1;
    Real J2 = hamiltonian_parameter.J2;
    Real Jc = hamiltonian_parameter.Jc;
    Real theta1 = hamiltonian_parameter.theta1;
    Real theta2 = hamiltonian_parameter.theta2;
    
    spdlog::info("J1 = {}, J2 = {}, Jc = {}, theta1 = {}, theta2 = {}", J1, J2, Jc, theta1, theta2);
}

void show(SweepParameter const & sweep_parameter) {
    Int maxdim = sweep_parameter.maxdim;
    Int mindim = sweep_parameter.mindim;
    Int niter = sweep_parameter.niter;
    Real cutoff = sweep_parameter.cutoff;
    Real noise = sweep_parameter.noise;

    spdlog::info("maxdim = {}, mindim = {}, cutoff = {}, niter = {}, noise = {}", maxdim, mindim, cutoff, niter, noise);
}

void show(RunParameter const & run_parameter) {
    bool quiet = run_parameter.quiet;
    spdlog::info("quiet = {}", quiet);
    for (auto const & sweep : run_parameter.sweeps) {
        show(sweep);
    }
}

int unsafe_main(int argc, char **argv);

int unsafe_main(int argc, char **argv)
{
    namespace IT = itensor;
    auto console = spdlog::stdout_color_mt("console");

    std::string system_filename = "system.toml";
    std::string run_filename = "run.toml";
    bool restart = false;
    Int n_excited = 2;

    {
        CLI::App app{std::string("S12c_dmrg, version git-") + GIT_REV};
        bool version = false;
        app.add_option("system_file", system_filename, "system toml")->required(true);
        app.add_option("run_file", run_filename, "run toml")->required(true);
        app.add_flag("-r,--restart", restart, "Restart or not");
        app.add_flag("-v,--version", version, "Show version");
        app.add_option("-e,--n_excited", n_excited, "Number of excited states");
        CLI11_PARSE(app, argc, argv);

        if (version) {
            fmt::print("{}, version git-{}\n", argv[0], GIT_REV);
            return 0;
        }
    }

    LatticeParameter lattice_parameter;
    HamiltonianParameter hamiltonian_parameter;
    RunParameter run_parameter;
    {
        spdlog::info("Reading system file {}", system_filename);
        auto system_file = cpptoml::parse_file(system_filename);
        if (!system_file) {
            spdlog::error("system file missing");
            exit(1);
        } else {
            spdlog::info("file OK");
        }

        spdlog::info("Getting lattice parameters");
        {
            auto p = system_file->get_table("lattice");
            if (!p) {
                spdlog::error("lattice section missing");
                exit(1);
            }
            lattice_parameter = read_lattice(*p);
        }

        spdlog::info("Getting hamiltonian parameters");
        {
            auto p = system_file->get_table("hamiltonian");
            if (!p) {
                spdlog::error("hamiltonian section missing");
                exit(1);
            }
            hamiltonian_parameter = read_hamiltonian(*p);
        }

        spdlog::info("Reading run file {}", run_filename);
        auto run_file = cpptoml::parse_file(run_filename);
        if (!run_file) {
            spdlog::error("run file missing");
            exit(1);
        } else {
            spdlog::info("file OK");
        }

        spdlog::info("Getting run parameters");
        {
            auto p = run_file->get_table("run");
            if (!p) {
                spdlog::error("run section missing");
                exit(1);
            }
            run_parameter = read_run(*p);
        }
    }

    spdlog::info("Finished loading parameters");
    spdlog::info("Lattice:");
    show(lattice_parameter);
    spdlog::info("Hamiltonian:");
    show(hamiltonian_parameter);
    spdlog::info("Run:");
    show(run_parameter);

    spdlog::info("Start running");
    run(lattice_parameter, hamiltonian_parameter, run_parameter, n_excited);
    spdlog::info("Finished");
    return 0;
}

LatticeParameter
read_lattice(cpptoml::table const & input_lattice)
{
    LatticeParameter lattice;
    lattice.n1 = input_lattice.get_as<Int>("n1").value_or(-1);
    lattice.n2 = input_lattice.get_as<Int>("n2").value_or(-1);
    lattice.periodic1 = input_lattice.get_as<bool>("periodic1").value_or(false);
    lattice.periodic2 = input_lattice.get_as<bool>("periodic2").value_or(false);
    return lattice;
}

HamiltonianParameter
read_hamiltonian(cpptoml::table const & input_hamiltonian)
{
    HamiltonianParameter hamiltonian;
    hamiltonian.J1 = input_hamiltonian.get_as<Real>("J1").value_or(0.0);
    hamiltonian.J2 = input_hamiltonian.get_as<Real>("J2").value_or(0.0);
    hamiltonian.Jc = input_hamiltonian.get_as<Real>("Jc").value_or(0.0);
    hamiltonian.theta1 = input_hamiltonian.get_as<Real>("theta1").value_or(0.0);
    hamiltonian.theta2 = input_hamiltonian.get_as<Real>("theta2").value_or(0.0);
    return hamiltonian;
}

RunParameter
read_run(cpptoml::table const & input_run)
{
    auto input_sweeps = input_run.get_table_array("sweeps");
    Int nsweeps = std::distance(input_sweeps->begin(), input_sweeps->end());
    RunParameter runparam;
    for (auto const & row : *input_sweeps) {
        SweepParameter p;
        p.maxdim = row->get_as<Int>("maxdim").value_or(0);
        p.mindim = row->get_as<Int>("mindim").value_or(0);
        p.cutoff = row->get_as<Real>("cutoff").value_or(0);
        p.niter = row->get_as<Int>("niter").value_or(0);
        p.noise = row->get_as<Real>("noise").value_or(0);
        runparam.sweeps.push_back(p);
    }
    runparam.quiet = input_run.get_as<bool>("quiet").value_or(false);
    return runparam;
}

itensor::Sweeps
convert_sweeps(std::vector<SweepParameter> const & sweeps_param)
{
    itensor::Sweeps sweeps(sweeps_param.size());
    for (int i = 0 ; i < sweeps_param.size() ; ++i) {
        sweeps.setmaxdim(i+1, sweeps_param[i].maxdim);
        sweeps.setmindim(i+1, sweeps_param[i].mindim);
        sweeps.setcutoff(i+1, sweeps_param[i].cutoff);
        sweeps.setniter(i+1, sweeps_param[i].niter);
        sweeps.setcutoff(i+1, sweeps_param[i].cutoff);
    }
    return sweeps;
}

       
void
run(LatticeParameter const & lattice_parameter,
    HamiltonianParameter const & hamiltonian_parameter,
    RunParameter const & run_parameter,
    Int n_excited) {

    namespace IT = itensor;
    using namespace itensor;

    auto [n1, n2, periodic1, periodic2] = lattice_parameter;
    auto [J1, J2, Jc, theta1, theta2] = hamiltonian_parameter;
    auto [quiet, sweep_parameter] = run_parameter;

    auto sweeps = convert_sweeps(sweep_parameter);

    using std::filesystem::is_regular_file;

    Int n_sites = n1 * n2;
    auto bonds = s12c::make_bonds(n1, n2, periodic1, periodic2);

    IT::SpinHalf sites;
    IT::MPS psi;

    bool read = false;
    if (!is_regular_file("sites.itn") || !is_regular_file("psi.itn")) {
        spdlog::error("file does not exist.");
        exit(1);
    }

    spdlog::info("Reading from file sites.itn");
    readFromFile("sites.itn", sites);

    auto neel_state = IT::InitState(sites);
    for (Int i1 = 0 ; i1 < n1 ; ++i1) {
      for (Int i2 = 0 ; i2 < n2 ; ++i2) { 
        Int i = i1*n2 + i2 + 1;
        neel_state.set(i, ((i1+i2)%2 == 0) ? "Up" : "Dn");
      }
    }

    psi = IT::MPS(sites);

    spdlog::info("Reading from file psi.itn");
    readFromFile("psi.itn", psi);

    spdlog::info("n1={}, n2={}, p1={}, p2={}", n1, n2, periodic1, periodic2);
    spdlog::info("J1={:.3f}, J2={:.3f}, Jc={:.3f}", J1, J2, Jc);
    spdlog::info("theta1={:.3f}, theta2={:.3f}", theta1, theta2);

    IT::MPO hamiltonian = s12c::make_hamiltonian(sites, bonds, J1, J2, Jc, theta1, theta2);
    spdlog::info("Made Hamiltonian");

    IT::Args args = {"Quiet", quiet};
    //IT::DMRGObserver obs(psi, args);
    auto en0 = std::real(innerC(psi, hamiltonian, psi));
    std::vector<MPS> wavefunctions = { psi };

    for (Int i = 1 ; i <= n_excited ; ++i) {
      MPS psi1(sites);
      auto filename = fmt::format("psi_{:02d}.itn", i);
      if (is_regular_file(filename)) {
          spdlog::info("Reading from file {}", filename);
          readFromFile(filename, psi1);
      } else {
          psi1 = randomMPS(neel_state);
      }

      Real en1 = dmrg(psi1, hamiltonian, wavefunctions, sweeps, {"Quiet=", true, "Weight=", 100.0});
      spdlog::info("Ground state energy per site = {:.20f}", en0 / n_sites);
      spdlog::info("Excited state energy per site = {:.20f}", en1 / n_sites);
      for (auto const & wf : wavefunctions) {
        spdlog::info("Overlaps = {:.20f}", std::abs(innerC(wf, psi1)));
      }
      spdlog::info("Saving to file");
      IT::writeToFile(filename, psi1);
      wavefunctions.push_back(psi1);
    }
}

int main(int argc, char **argv) {
  try {
    return unsafe_main(argc, argv);
  } catch (std::exception const & e) {
    std::cerr << e.what() << std::endl;
    throw e;
  }
}

