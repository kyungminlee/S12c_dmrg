#pragma once

#include <cmath>

#include <utility>
#include <vector>
#include <tuple>
#include <iostream>

#include <itensor/all.h>
#include <xtensor/xarray.hpp>

#include "spdlog/spdlog.h"

namespace s12c {

using float64_t = double;
using complex128_t = std::complex<double>;

using Int = int32_t;
using Real = float64_t;
using Complex = complex128_t;



struct LatticeParameter {
    Int n1;
    Int n2;
    bool periodic1;
    bool periodic2;
};

struct HamiltonianParameter {
    Real J1;
    Real J2;
    Real Jc;
    Real theta1;
    Real theta2;
};

struct SweepParameter {
    Int maxdim;
    Int mindim;
    Int niter;
    Real cutoff;
    Real noise;    
};


struct RunParameter {
    bool quiet;
    std::vector<SweepParameter> sweeps;
};









// Returns a tuple of (sites, nn, nnn, chiral)
using IntPair = std::pair<Int, Int>;
using RealVector = xt::xarray<Real>;
using BondType = std::tuple<Int, Int, RealVector>;
using TripletType = std::tuple<Int, Int, Int, RealVector, RealVector>;

using AllBondType = std::tuple< std::vector<BondType>, std::vector<BondType>, std::vector<TripletType> >;

AllBondType make_bonds(Int const n1, Int const n2, bool const periodic1=false, bool const periodic2=false);

itensor::MPO
make_hamiltonian(itensor::SpinHalf const & sites,
                    AllBondType const & bonds,
                    Real J1, Real J2, Real Jc,
                    Real theta1=0.0, Real theta2=0.0);

inline AllBondType
make_bonds(Int const n1, Int const n2, bool const periodic1, bool const periodic2) {
    using namespace std;
    using namespace itensor;
    Int const n_sites = n1 * n2;

    using IntPair = pair<Int, Int>;
    vector<IntPair> nearest_neighbors = {{1, 0},
                                         {0, 1}};
    vector<IntPair> second_nearest_neighbors = {{1,  1},
                                                {-1, 1}};
    vector<pair<IntPair, IntPair>> chiral_triplets = {
            {{1,  0},  {0,  1}},
            {{0,  1},  {-1, 0}},
            {{-1, 0},  {0,  -1}},
            {{0,  -1}, {1,  0}}
    };

    vector<BondType> nearest_neighbor_bonds;
    vector<BondType> second_nearest_neighbor_bonds;
    vector<TripletType> chiral_triplet_bonds;

    auto s2i = [n1, n2](Int i1, Int i2) { return (i1 % n1) * n2 + (i2 % n2) + 1; };

    for (Int i1 = 0; i1 < n1; ++i1) {
        for (Int i2 = 0; i2 < n2; ++i2) {
            Int i = s2i(i1, i2);
            for (auto const &dis : nearest_neighbors) {
                Int j1 = i1 + dis.first, j2 = i2 + dis.second;
                if (periodic1) { j1 = ((j1 + n1 * n2) % n1); }
                if (periodic2) { j2 = ((j2 + n1 * n2) % n2); }

                if (j1 < 0 || j1 >= n1 || j2 < 0 || j2 >= n2) { continue; }
                Int j = s2i(j1, j2);
                RealVector dis_vec = {static_cast<double>(dis.first), static_cast<double>(dis.second)};
                nearest_neighbor_bonds.emplace_back(i, j, dis_vec);
            }
            for (auto const &dis : second_nearest_neighbors) {
                Int j1 = i1 + dis.first, j2 = i2 + dis.second;
                if (periodic1) { j1 = ((j1 + n1 * n2) % n1); }
                if (periodic2) { j2 = ((j2 + n1 * n2) % n2); }
                if (j1 < 0 || j1 >= n1 || j2 < 0 || j2 >= n2) { continue; }
                Int j = s2i(j1, j2);
                RealVector dis_vec = {static_cast<double>(dis.first), static_cast<double>(dis.second)};
                second_nearest_neighbor_bonds.emplace_back(i, j, dis_vec);
            }
            for (auto const &trip : chiral_triplets) {
                auto const &dis1 = get<0>(trip);
                auto const &dis2 = get<1>(trip);

                Int j1 = i1 + dis1.first, j2 = i2 + dis1.second;
                Int k1 = i1 + dis2.first, k2 = i2 + dis2.second;
                if (periodic1) {
                    j1 = ((j1 + n1 * n2) % n1);
                    k1 = ((k1 + n1 * n2) % n1);
                }
                if (periodic2) {
                    j2 = ((j2 + n1 * n2) % n2);
                    k2 = ((k2 + n1 * n2) % n2);
                }
                if (j1 < 0 || j1 >= n1 || j2 < 0 || j2 >= n2) { continue; }
                if (k1 < 0 || k1 >= n1 || k2 < 0 || k2 >= n2) { continue; }
                Int j = s2i(j1, j2);
                Int k = s2i(k1, k2);
                RealVector dis1_vec = {static_cast<double>(dis1.first), static_cast<double>(dis1.second)};
                RealVector dis2_vec = {static_cast<double>(dis2.first), static_cast<double>(dis2.second)};

                chiral_triplet_bonds.emplace_back(i, j, k, dis1_vec, dis2_vec);
            }
        }
    }
    return make_tuple(nearest_neighbor_bonds, second_nearest_neighbor_bonds, chiral_triplet_bonds);
}


inline itensor::MPO
make_hamiltonian(itensor::SpinHalf const & sites,
                    AllBondType const & bonds,
                    Real J1, Real J2, Real Jc,
                    Real theta1, Real theta2)
{
    namespace IT = itensor;
    auto const & [nearest_neighbors, second_nearest_neighbors, chiral_triplets] = bonds;
    auto n1 = 1, n2 = 1;
    Real theta_tilde_1 = 2 * M_PI * theta1 / n1;
    Real theta_tilde_2 = 2 * M_PI * theta2 / n2;
    
    auto ampo = IT::AutoMPO(sites);
    auto twistangle = [theta_tilde_1, theta_tilde_2](auto const & dis) -> Complex {
        Real phi = dis[0] * theta_tilde_1 + dis[1] * theta_tilde_2;
        return Complex(std::cos(phi), std::sin(phi));
    };

    if (abs(J1) > 0) {
        spdlog::info("Adding J1 terms");
        for (auto &[i, j, dis] : nearest_neighbors) {
            ampo += (0.5 * J1 * twistangle(-dis)), "S+", i, "S-", j;
            ampo += (0.5 * J1 * twistangle(+dis)), "S-", i, "S+", j;
            ampo += J1, "Sz", i, "Sz", j;
        }
    }

    if (abs(J2) > 0) {
        spdlog::info("Adding J2 terms");
        for (auto &[i, j, dis] : second_nearest_neighbors) {
            ampo += (0.5 * J2 * twistangle(-dis)), "S+", i, "S-", j;
            ampo += (0.5 * J2 * twistangle(+dis)), "S-", i, "S+", j;
            ampo += J2, "Sz", i, "Sz", j;
        }
    }

    if (abs(Jc) > 0) {
        spdlog::info("Adding Jc terms");
        static const std::complex<double> I(0.0, 1.0);
        for (auto &[i, j, k, d, e] : chiral_triplets) {
            ampo += (+0.5 * I * Jc * twistangle(d-e)), "Sz", i, "S+", j, "S-", k;
            ampo += (-0.5 * I * Jc * twistangle(e-d)), "Sz", i, "S-", j, "S+", k;

            ampo += (+0.5 * I * Jc * twistangle(+e)), "Sz", j, "S+", k, "S-", i;
            ampo += (-0.5 * I * Jc * twistangle(-e)), "Sz", j, "S-", k, "S+", i;

            ampo += (+0.5 * I * Jc * twistangle(-d)), "Sz", k, "S+", i, "S-", j;
            ampo += (-0.5 * I * Jc * twistangle(+d)), "Sz", k, "S-", i, "S+", j;
        }
    }
    spdlog::info("Finished adding terms.");
    return toMPO(ampo);
}



}
