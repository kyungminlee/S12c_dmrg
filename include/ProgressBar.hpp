#ifndef PROGRESSBAR_PROGRESSBAR_HPP
#define PROGRESSBAR_PROGRESSBAR_HPP

#include <chrono>
#include <iostream>

class ProgressBar {
private:
    unsigned int ticks = 0;

    const unsigned int total_ticks;
    const unsigned int bar_width;
    const char complete_char = '=';
    const char incomplete_char = ' ';
    const std::chrono::steady_clock::time_point start_time = std::chrono::steady_clock::now();

    std::ostream & output_stream;
public:
    ProgressBar(unsigned int total, unsigned int width, char complete, char incomplete, std::ostream & stream = std::cerr) :
            total_ticks {total}, bar_width {width}, complete_char {complete}, incomplete_char {incomplete}, output_stream {stream} {}

    ProgressBar(unsigned int total, unsigned int width, std::ostream & stream = std::cerr) : total_ticks {total}, bar_width {width}, output_stream {stream} {}

    unsigned int operator++() { return ++ticks; }

    void display() const
    {
        float progress = (float) ticks / total_ticks;
        int pos = (int) (bar_width * progress);

        std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
        auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now-start_time).count();

        output_stream << "[";

        for (int i = 0; i < bar_width; ++i) {
            if (i < pos) output_stream << complete_char;
            else if (i == pos) output_stream << ">";
            else output_stream << incomplete_char;
        }
        output_stream << "] " << int(progress * 100.0) << "% "
                  << float(time_elapsed) / 1000.0 << "s\r";
        output_stream.flush();
    }

    void done() const
    {
        display();
        output_stream << std::endl;
    }
};

#endif //PROGRESSBAR_PROGRESSBAR_HPP
