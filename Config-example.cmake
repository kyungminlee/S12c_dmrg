if (WIN32)
    set(ITENSOR_INCLUDE_DIR "$ENV{USERPROFILE}/local/lib/itensor/include")
    set(ITENSOR_LIB_DIR "$ENV{USERPROFILE}/local/lib/itensor/lib/${CMAKE_BUILD_TYPE}")
    set(INTEL_ROOT "C:\\Program Files (x86)\\IntelSWTools\\compilers_and_libraries\\windows")
else()
    set(ITENSOR_INCLUDE_DIR "$ENV{HOME}/.local/pkg/itensor-3.1.1/include")
    set(ITENSOR_LIB_DIR "$ENV{HOME}/.local/pkg/itensor-3.1.1/lib")

    set(BLA_VENDOR "Intel10_64lp")
	set(INTEL_ROOT /gpfs/research/software/intel-2016-2/compilers_and_libraries/linux)
    set(ENV{MKLROOT} "${INTEL_ROOT}/mkl")

    set(CMAKE_SKIP_BUILD_RPATH FALSE)
    set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

    #list(APPEND CMAKE_INSTALL_RPATH "$ENV{HOME}/.local/pkg/easybuild/software/GCCcore/9.2.0/lib64")
    list(APPEND CMAKE_INSTALL_RPATH "/opt/hpc/gnu-8.2.1/lib64")
    list(APPEND CMAKE_INSTALL_RPATH "${ITENSOR_LIB_DIR}")
    list(APPEND CMAKE_INSTALL_RPATH "${INTEL_ROOT}/mkl/lib/intel64" "${INTEL_ROOT}/tbb/lib/intel64" "${INTEL_ROOT}/lib/intel64")

    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif()
